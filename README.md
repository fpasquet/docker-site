Install docker
==============

```
echo '>>> Running apt-get update'
sudo apt-get update >/dev/null 2>&1
echo 'Finished running apt-get update'

echo '>>> Installing docker'
curl -sSL https://get.docker.com/ | sh >/dev/null 2>&1
echo 'Finished installing docker'

echo '>>> Created a Docker group'
sudo usermod -aG docker vagrant
echo 'Finished created Docker group'

echo '>>> Install Docker Compose'
sudo bash -c "curl -L https://github.com/docker/compose/releases/download/1.5.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose"
sudo chmod +x /usr/local/bin/docker-compose
echo 'Finished created Docker group'
```

```
sudo docker-compose run site bash
```